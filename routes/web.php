<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});


Route::get('/nama-url', function () {
    echo "this is just a page for experimental purpose<br>";
    return "<h1>welcome pots</h1>";
});

Route::get('/halo/{name}', function($name){
  return "<h1>Hallo $name</h1>";
});

Route::get('/home', 'attempt1_controller@coba');

Route::get('/', 'HomeController@home_page');
*/
Route::get('/register', 'AuthController@register_page');

Route::post('/welcomepage', 'AuthController@welcome_page');

Route::get('/master', function(){
  return view('adminlte.master');
});

Route::get('/', function(){
  return view('adminlte.core.index');
});

Route::get('/data-tables', function(){
  return view('adminlte.core.data-tables');
});

//TUGAS CRUD!!
Route::get('/pertanyaan/create','Postcontroller@create');
Route::post('/pertanyaan','Postcontroller@store');
Route::get('/pertanyaan','Postcontroller@index');
Route::get('/pertanyaan/{pertanyaan_id}','Postcontroller@show');
Route::get('/pertanyaan/{pertanyaan_id}/edit','Postcontroller@edit');
Route::put('/pertanyaan/{pertanyaan_id}','Postcontroller@update');
Route::delete('/pertanyaan/{pertanyaan_id}','Postcontroller@destroy');
