@extends('adminlte.master')

@section('content')

          <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Ubah Pertanyaan dengan id: {{$pertanyaan->id}}</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="/pertanyaan/{{$pertanyaan->id}}" method="POST">
                @csrf
                @method('PUT')
                <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Judul</label>
                    <input type="text" class="form-control" id="judul" name="judul" value="{{ old('judul', $pertanyaan->judul)}}" placeholder="Ketik Judul">
                    @error('judul')
                      <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Isi Pertanyaan</label>
                    <input type="text" class="form-control" id="isi" name="isi" value="{{ old('isi', $pertanyaan->isi)}}" placeholder="Ketik Isi Pertanyaan">
                    @error('isi')
                      <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Simpan Perubahan</button>
                  <a href="/pertanyaan" class="btn btn-primary">Kembali</a>
                </div>
              </form>
          </div>

@endsection
