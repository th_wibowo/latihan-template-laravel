<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class Postcontroller extends Controller
{
    public function create(){
      return view('belajarcrud.create');
    }

    public function store(Request $request){
      //dd($request->all());
      $request->validate([
        'judul' => 'required|unique:pertanyaan',
        'isi' => 'required'
      ]);

      $query = DB::table('pertanyaan')->insert([
        "judul" => $request["judul"],
        "isi" => $request["isi"]
      ]);

      return redirect('/pertanyaan')->with('success', 'Pertanyaan berhasil disimpan! :)');
    }

    public function index(){
      $pertanyaan = DB::table('pertanyaan')->get();
      //dd($pertanyaan);
      return view('belajarcrud.index', compact('pertanyaan'));
    }

    public function show($id){
      $pertanyaan = DB::table('pertanyaan')->where('id', $id)->first(); //tidak menggunakan get() karena akan mengembalikan data dalam bentuk array(kumpulan objek), bukan suatu objek
      return view('belajarcrud.show', compact('pertanyaan'));
    }

    public function edit($id){
      $pertanyaan = DB::table('pertanyaan')->where('id', $id)->first();
      return view('belajarcrud.edit', compact('pertanyaan'));
    }

    public function update($id, Request $request){
      $query = DB::table('pertanyaan')
                    ->where('id', $id)
                    ->update([
                      'judul' => $request['judul'],
                      'isi' => $request['isi']
                    ]);
      return redirect('/pertanyaan')->with('success', 'Pertanyaan berhasil diubah! :)');
    }

    public function destroy($id){
      $query = DB::table('pertanyaan')->where('id', $id)->delete();
      return redirect('/pertanyaan')->with('success', 'Pertanyaan berhasil dihapus! :)');
    }
}
