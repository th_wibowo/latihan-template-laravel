@extends('adminlte.master')

@section('content')

<div class="card">
  <div class="card-header">
    <h3 class="card-title">Detail Pertanyaan id: {{$pertanyaan->id}}</h3>
  </div>
    <!-- /.card-header -->
    <div class="card-body">
      <h4> {{ $pertanyaan->judul }} </h4>
      <p> {{ $pertanyaan->isi }} </p>
    </div>
    <a href="/pertanyaan" class="btn btn-primary" style="margin: 20px 0px;">Kembali</a>
    <!-- /.card-body -->
</div>

@endsection
