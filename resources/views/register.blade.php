<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Form Sign Up</title>
  </head>
  <body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form name="form_signup" action="/welcomepage" method="POST">
      @csrf

      <p>First Name:</p>
        <input type="text" name="nama_depan" size="25">
      <p>Last Name:</p>
        <input type="text" name="nama_belakang" size="25">
      <p>Gender:</p>
        <input type="radio" name="gender" value="male">
        <label for="male">Male</label><br>
        <input type="radio" name="gender" value="female">
        <label for="female">Female</label><br>
        <input type="radio" name="gender" value="other">
        <label for="other">Other</label><br>
      <p>Nationality:</p>
        <select name="nationality">
          <option value="indonesian">Indonesian</option>
          <option value="singaporean">Singaporean</option>
          <option value="chinese">Chinese</option>
          <option value="filipinos">Filipinos</option>
          <option value="malaysian">Malaysian</option>
          <option value="thai">Thai</option>
          <option value="rusian">Rusian</option>
          <option value="american">American</option>
        </select>
      <p>Language Spoken:</p>
        <input type="checkbox" name="language" value="bahasa">
        <label for="bahasa">Bahasa Indonesia</label><br>
        <input type="checkbox" name="language" value="english">
        <label for="english">English</label><br>
        <input type="checkbox" name="language" value="other">
        <label for="other">Other</label>
      <p>Bio:</p>
        <textarea name="biodata" rows="10" cols="40"></textarea><br>
      <input type="submit" name="button_signup" value="Sign Up">
    </form>
  </body>
</html>
